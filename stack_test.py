import unittest
from stack import Stack


class StackTest(unittest.TestCase):
    def setUp(self):
        self.s = Stack()

    def test_empty(self):
        self.assertTrue(self.s.is_empty())
        self.assertEqual(0, self.s.size())

    def test_empty_with_item_in_stack(self):
        self.s.push(1)
        self.assertFalse(self.s.is_empty())

    def test_clear_for_empty_stack(self):
        self.s.clear()
        self.assertTrue(self.s.is_empty())

    def test_clear_with_item_in_stack(self):
        self.s.push(1)
        self.s.clear()
        self.assertTrue(self.s.is_empty())

    def test_push_for_one_item(self):
        self.s.push(1)
        self.assertFalse(self.s.is_empty())
        self.assertEqual(1, self.s.size())

    def test_push_mul_items(self):
        arr = [1,2,3,4,5,6,7,8]
        for i in arr:
            self.s.push(i)
        self.assertEqual(len(arr), self.s.size())

    def test_pop(self):
        self.s.push(1)
        self.assertEqual(1, self.s.pop())

    def test_pop_with_empty_stack(self):
        with self.assertRaises(IndexError) as err:
            self.s.pop()
        self.assertTrue("Stack is Empty" in err.exception.args)

    def test_peek(self):
        self.s.push(1)
        len_stack = self.s.size()
        self.assertEqual(1, self.s.peek())
        self.assertEqual(len_stack, self.s.size())

    def test_peek_with_empty_stack(self):
        self.assertIsNone(self.s.peek())

    def test_FIFO(self):
        arr = [1,2,3,4,5,6,7,8]
        for i in arr:
            self.s.push(i)
        for i in range(len(arr)):
            self.assertEqual(arr[len(arr) - i - 1], self.s.pop())

        self.assertTrue(self.s.is_empty())

def main():
    unittest.main()

if __name__ == '__main__':
    main()
    