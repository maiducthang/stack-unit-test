import unittest

class Stack:
    def __init__(self):
        self.items = []

    def pop(self):
        if self.is_empty():
            raise IndexError("Stack is Empty")
        else:
            return self.items.pop()

    def push(self, item):
        self.items.append(item)

    def peek(self):
        if self.is_empty():
            return None
        return self.items[-1]

    def clear(self):
        self.items.clear()

    def is_empty(self):
        return len(self.items) == 0

    def size(self):
        return len(self.items)


def main():
    a = Stack()
    a.push([1,2,3,4,5])
    print(a.size())
if __name__ == '__main__':
    main()
    